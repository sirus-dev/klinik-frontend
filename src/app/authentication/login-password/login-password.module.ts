import {NgModule} from '@angular/core';
import {LoginPasswordComponent} from './login-password.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {LoginPasswordRoutes} from './login-password.routing';
import {SharedModule} from '../../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LoginPasswordRoutes),
        SharedModule
    ],
    declarations: [LoginPasswordComponent]
})

export class LoginPasswordModule {}
