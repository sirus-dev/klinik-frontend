import {Routes} from '@angular/router';
import {LoginEmailComponent} from './login-email.component';

export const LoginEmailRoutes: Routes = [
    {
        path: '',
        component: LoginEmailComponent,
        data: {
            breadcrumb: 'Login'
        }
    }
];
