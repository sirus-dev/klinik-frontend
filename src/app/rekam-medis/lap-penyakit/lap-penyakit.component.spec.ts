import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LapPenyakitComponent } from './lap-penyakit.component';

describe('LapPenyakitComponent', () => {
  let component: LapPenyakitComponent;
  let fixture: ComponentFixture<LapPenyakitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LapPenyakitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LapPenyakitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
