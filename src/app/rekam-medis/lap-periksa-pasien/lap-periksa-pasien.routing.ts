import { Routes } from '@angular/router';

import { LapPeriksaPasienComponent } from './lap-periksa-pasien.component';

export const LapPeriksaPasienRoutes: Routes = [{
  path: '',
  component: LapPeriksaPasienComponent,
  data: {
    breadcrumb: 'Laporan Pemeriksaan Poliklinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];