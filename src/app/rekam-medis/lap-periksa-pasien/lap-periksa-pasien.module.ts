import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { LapPeriksaPasienComponent } from './lap-periksa-pasien.component';
import { LapPeriksaPasienRoutes } from './lap-periksa-pasien.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(LapPeriksaPasienRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [LapPeriksaPasienComponent]
})

export class LapPeriksaPasienModule {}
