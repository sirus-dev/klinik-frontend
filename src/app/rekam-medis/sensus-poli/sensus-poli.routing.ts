import { Routes } from '@angular/router';

import { SensusPoliComponent } from './sensus-poli.component';

export const SensusPoliRoutes: Routes = [{
  path: '',
  component: SensusPoliComponent,
  data: {
    breadcrumb: 'Sensus Poliklinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];