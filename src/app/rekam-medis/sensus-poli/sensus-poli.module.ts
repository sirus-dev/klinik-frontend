import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { SensusPoliComponent } from './sensus-poli.component';
import { SensusPoliRoutes } from './sensus-poli.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(SensusPoliRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [SensusPoliComponent]
})

export class SensusPoliModule {}
