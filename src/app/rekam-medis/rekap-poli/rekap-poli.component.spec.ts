import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RekapPoliComponent } from './rekap-poli.component';

describe('RekapPoliComponent', () => {
  let component: RekapPoliComponent;
  let fixture: ComponentFixture<RekapPoliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapPoliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RekapPoliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
