import { Routes } from '@angular/router';

import { KonsulTindakPoliComponent } from './konsul-tindak-poli.component';

export const KonsulTindakPoliRoutes: Routes = [{
  path: '',
  component: KonsulTindakPoliComponent,
  data: {
    breadcrumb: 'Sensus Poliklinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];