import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-konsul-tindak-poli',
  templateUrl: './konsul-tindak-poli.component.html',
  styleUrls: ['./konsul-tindak-poli.component.css']
})
export class KonsulTindakPoliComponent implements OnInit {

  constructor( private _location: Location) {}
  
  backClicked() {
    this._location.back();
    }

  refClicked() {
    location.reload();
  }

  ngOnInit() {
  }

}
