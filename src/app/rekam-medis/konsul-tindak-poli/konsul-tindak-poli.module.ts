import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KonsulTindakPoliComponent } from './konsul-tindak-poli.component';
import { KonsulTindakPoliRoutes } from './konsul-tindak-poli.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KonsulTindakPoliRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [KonsulTindakPoliComponent]
})

export class KonsulTindakPoliModule {}
