import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RekamMedisComponent } from './rekam-medis.component';
import { RekamMedisRoutes } from './rekam-medis.routing';
import { PasienComponent } from './pasien/pasien.component';
import { SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { IdentitasPasienComponent } from './identitas-pasien/identitas-pasien.component';
import { SensusPoliComponent } from './sensus-poli/sensus-poli.component';
import { LapPeriksaPasienComponent } from './lap-periksa-pasien/lap-periksa-pasien.component';
import { JurnalPoliComponent } from './jurnal-poli/jurnal-poli.component';
import { RekapPoliComponent } from './rekap-poli/rekap-poli.component';
import { LapPenyakitComponent } from './lap-penyakit/lap-penyakit.component';
import { KonsulTindakPoliComponent } from './konsul-tindak-poli/konsul-tindak-poli.component';
import { DetailPeriksaPasienComponent } from './detail-periksa-pasien/detail-periksa-pasien.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RekamMedisRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [
    RekamMedisComponent,
    PasienComponent,
    IdentitasPasienComponent,
    SensusPoliComponent,
    LapPeriksaPasienComponent,
    JurnalPoliComponent,
    RekapPoliComponent,
    LapPenyakitComponent,
    KonsulTindakPoliComponent,
    DetailPeriksaPasienComponent
  ]
})

export class RekamMedisModule {}
