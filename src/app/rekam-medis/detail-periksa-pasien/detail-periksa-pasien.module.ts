import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DetailPeriksaPasienComponent } from './detail-periksa-pasien.component';
import { DetailPeriksaPasienRoutes } from './detail-periksa-pasien.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(DetailPeriksaPasienRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [DetailPeriksaPasienComponent]
})

export class DetailPeriksaPasienModule {}
