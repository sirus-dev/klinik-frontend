import { Routes } from '@angular/router';

import { DetailPeriksaPasienComponent } from './detail-periksa-pasien.component';

export const DetailPeriksaPasienRoutes: Routes = [{
  path: '',
  component: DetailPeriksaPasienComponent,
  data: {
    breadcrumb: 'Detail Pemeriksaan Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
