import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JurnalPoliComponent } from './jurnal-poli.component';

describe('JurnalPoliComponent', () => {
  let component: JurnalPoliComponent;
  let fixture: ComponentFixture<JurnalPoliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JurnalPoliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JurnalPoliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
