import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PasienComponent } from './pasien.component';
import { PasienRoutes } from './pasien.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PasienRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PasienComponent]
})

export class PasienModule {}
