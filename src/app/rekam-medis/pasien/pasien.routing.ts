import { Routes } from '@angular/router';

import { PasienComponent } from './pasien.component';

export const PasienRoutes: Routes = [{
  path: '',
  component: PasienComponent,
  data: {
    breadcrumb: 'Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
