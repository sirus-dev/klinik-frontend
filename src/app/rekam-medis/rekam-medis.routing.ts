import { Routes } from '@angular/router';
import {RekamMedisComponent} from './rekam-medis.component';
import {PasienComponent} from './pasien/pasien.component';
import { IdentitasPasienComponent } from './identitas-pasien/identitas-pasien.component';
import { SensusPoliComponent } from './sensus-poli/sensus-poli.component';
import { KonsulTindakPoliComponent } from './konsul-tindak-poli/konsul-tindak-poli.component';
import { LapPeriksaPasienComponent } from './lap-periksa-pasien/lap-periksa-pasien.component';
import { DetailPeriksaPasienComponent } from './detail-periksa-pasien/detail-periksa-pasien.component';



export const RekamMedisRoutes: Routes = [
  {
    path: '',
    data: {
        breadcrumb: 'Rekam Medis',
        status: false
    },
    children: [
        {
            path: '',
            component: RekamMedisComponent,
            data: {
                breadcrumb: 'Menu',
                status: true
            } 
        }, {
            path: 'pasien',
            component: PasienComponent,
            data: {
                breadcrumb: 'Pasien',
                status: true
            } 
        },  {
            path: 'identitas-pasien',
            component: IdentitasPasienComponent,
            data: {
                breadcrumb: 'Identitas Pasien',
                status: true
            } 
        },  {
            path: 'sensus-poli',
            component: SensusPoliComponent,
            data: {
                breadcrumb: 'Sensus Poliklinik',
                status: true
            } 
        },  {
            path: 'konsul-tindak-poli',
            component: KonsulTindakPoliComponent,
            data: {
                breadcrumb: 'Konsultasi dan Tindakan Poliklinik',
                status: true
            } 
        }, {
            path: 'lap-periksa-pasien',
            component: LapPeriksaPasienComponent,
            data: {
                breadcrumb: 'Laporan Pemeriksaan Poliklinik',
                status: true
            } 
        }, {
            path: 'detail-periksa-pasien',
            component: DetailPeriksaPasienComponent,
            data: {
                breadcrumb: 'Detail Pemeriksaan Pasien',
                status: true
            } 
        }, 
    ]
  }
];
