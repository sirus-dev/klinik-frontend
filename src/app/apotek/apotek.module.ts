import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { ApotekComponent } from './apotek.component';
import { ApotekRoutes } from './apotek.routing';
import { PenjualanComponent } from './penjualan/penjualan.component';
import { SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(ApotekRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [
    ApotekComponent,
    PenjualanComponent
  ]
})

export class ApotekModule {}
