import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PenjualanComponent } from './penjualan.component';
import { PenjualanRoutes } from './penjualan.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PenjualanRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PenjualanComponent]
})

export class PenjualanModule {}
