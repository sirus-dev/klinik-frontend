import { Routes } from '@angular/router';

import { PenjualanComponent } from './penjualan.component';

export const PenjualanRoutes: Routes = [{
  path: '',
  component: PenjualanComponent,
  data: {
    breadcrumb: 'Penjualan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
