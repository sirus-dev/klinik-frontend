import { Routes } from '@angular/router';
import {ApotekComponent} from './apotek.component';
import {PenjualanComponent} from './penjualan/penjualan.component'; 

export const ApotekRoutes: Routes = [
  {
    path: '',
    data: {
        breadcrumb: 'Apotek',
        status: false
    },
    children: [
        {
            path: '',
            component: ApotekComponent,
            data: {
                breadcrumb: 'Menu',
                status: true
            } 
        }, {
            path: 'penjualan',
            component: PenjualanComponent,
            data: {
                breadcrumb: 'Penjualan',
                status: true
            } 
        }
    ]
  }
];
