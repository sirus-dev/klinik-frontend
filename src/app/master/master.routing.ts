import { Routes } from '@angular/router';
import {MasterComponent} from './master.component';
import {KaryawanComponent} from './karyawan/karyawan.component';
import {DokterComponent} from './dokter/dokter.component';
import {PerusahaanComponent} from './perusahaan/perusahaan.component';
import {KlasifikasiComponent} from './klasifikasi/klasifikasi.component';
import {PoliComponent} from './poli/poli.component';
import {PenyakitComponent} from './penyakit/penyakit.component';
import {Penyakit2Component} from './penyakit2/penyakit2.component';
import {PerawatComponent} from './perawat/perawat.component';

export const MasterRoutes: Routes = [
  {
    path: '',
    data: {
        breadcrumb: 'Master Data',
        status: false
    },
    children: [
        {
            path: '',
            component: MasterComponent,
            data: {
                breadcrumb: 'Menu',
                status: true
            } 
        }, {
            path: 'karyawan',
            component: KaryawanComponent,
            data: {
                breadcrumb: 'Data Karyawan',
                status: true
            } 
        }, {
            path: 'dokter',
            component: DokterComponent,
            data: {
                breadcrumb: 'Data Dokter',
                status: true
            }
        }, {
            path: 'perusahaan',
            component: PerusahaanComponent,
            data: {
                breadcrumb: 'Data Perusahaan',
                status: true
            }
        }, {
            path: 'klasifikasi',
            component: KlasifikasiComponent,
            data: {
                breadcrumb: 'Data Klasifikasi',
                status: true
            }
        }, {
            path: 'poli',
            component: PoliComponent,
            data: {
                breadcrumb: 'Data Poli',
                status: true
            }
        }, {
            path: 'penyakit',
            component: PenyakitComponent,
            data: {
                breadcrumb: 'Data Penyakit ( ICD 10 )',
                status: true
            }
        },  {
            path: 'penyakit2',
            component: Penyakit2Component,
            data: {
                breadcrumb: 'Data Penyakit ( ICD 9 )',
                status: true
            }
        }, {
            path: 'perawat',
            component: PerawatComponent,
            data: {
                breadcrumb: 'Data Perawat',
                status: true
            }
        },
    ]
  }
];
