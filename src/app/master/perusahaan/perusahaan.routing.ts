import { Routes } from '@angular/router';

import { PerusahaanComponent } from './perusahaan.component';

export const PerusahaanRoutes: Routes = [{
  path: '',
  component: PerusahaanComponent,
  data: {
    breadcrumb: 'Perusahaan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
