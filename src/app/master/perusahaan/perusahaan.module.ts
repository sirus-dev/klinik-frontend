import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PerusahaanComponent } from './perusahaan.component';
import { PerusahaanRoutes } from './perusahaan.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PerusahaanRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PerusahaanComponent]
})

export class PerusahaanModule {}
