import { Routes } from '@angular/router';

import { PoliComponent } from './poli.component';

export const PoliRoutes: Routes = [{
  path: '',
  component: PoliComponent,
  data: {
    breadcrumb: 'Poli',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
