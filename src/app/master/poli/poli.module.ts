import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PoliComponent } from './poli.component';
import { PoliRoutes } from './poli.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PoliRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PoliComponent]
})

export class PoliModule {}
