import { Routes } from '@angular/router';

import { KaryawanComponent } from './karyawan.component';

export const KaryawanRoutes: Routes = [{
  path: '',
  component: KaryawanComponent,
  data: {
    breadcrumb: 'Karyawan',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
