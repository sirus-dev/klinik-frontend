import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KaryawanComponent } from './karyawan.component';
import { KaryawanRoutes } from './karyawan.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KaryawanRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [KaryawanComponent]
})

export class KaryawanModule {}
