import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DokterComponent } from './dokter.component';
import { DokterRoutes } from './dokter.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(DokterRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [DokterComponent]
})

export class DokterModule {}