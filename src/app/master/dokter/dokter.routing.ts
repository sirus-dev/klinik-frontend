import { Routes } from '@angular/router';

import { DokterComponent } from './dokter.component';

export const DokterRoutes: Routes = [{
  path: '',
  component: DokterComponent,
  data: {
    breadcrumb: 'Dokter',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];