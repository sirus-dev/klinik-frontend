import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';

declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-dokter',
  templateUrl: './dokter.component.html'
})

export class DokterComponent implements OnInit {
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>; 
  public loading = false;

  constructor() { }

  ngOnInit() {
  }
}