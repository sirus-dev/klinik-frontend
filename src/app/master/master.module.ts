import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { MasterComponent } from './master.component';
import { MasterRoutes } from './master.routing';
import { KaryawanComponent } from './karyawan/karyawan.component';
import { SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { DokterComponent } from './dokter/dokter.component';
import { PerusahaanComponent } from './perusahaan/perusahaan.component';
import { KlasifikasiComponent } from './klasifikasi/klasifikasi.component';
import { PoliComponent } from './poli/poli.component';
import { PenyakitComponent } from './penyakit/penyakit.component';
import { Penyakit2Component } from './penyakit2/penyakit2.component';
import { PerawatComponent } from './perawat/perawat.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(MasterRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [
    MasterComponent,
    KaryawanComponent,
    DokterComponent,
    PerusahaanComponent,
    KlasifikasiComponent,
    PoliComponent,
    PenyakitComponent,
    Penyakit2Component,
    PerawatComponent
  ]
})

export class MasterModule {}
