import { Routes } from '@angular/router';

import { PenyakitComponent } from './penyakit.component';

export const PenyakitRoutes: Routes = [{
  path: '',
  component: PenyakitComponent,
  data: {
    breadcrumb: 'Penyakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
