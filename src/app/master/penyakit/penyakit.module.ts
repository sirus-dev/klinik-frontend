import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PenyakitComponent } from './penyakit.component';
import { PenyakitRoutes } from './penyakit.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PenyakitRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PenyakitComponent]
})

export class PerusahaanModule {}
