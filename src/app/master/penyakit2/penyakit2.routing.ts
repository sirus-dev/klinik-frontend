import { Routes } from '@angular/router';

import { Penyakit2Component } from './penyakit2.component';

export const Penyakit2Routes: Routes = [{
  path: '',
  component: Penyakit2Component,
  data: {
    breadcrumb: 'Penyakit ( ICD 9 )',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
