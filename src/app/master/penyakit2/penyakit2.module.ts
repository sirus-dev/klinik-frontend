import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { Penyakit2Component } from './penyakit2.component';
import { Penyakit2Routes } from './penyakit2.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(Penyakit2Routes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [Penyakit2Component]
})

export class Penyakit2Module {}
