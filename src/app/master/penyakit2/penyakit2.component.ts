import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';

declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-penyakit2',
  templateUrl: './penyakit2.component.html'
})

export class Penyakit2Component implements OnInit {
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>; 
  public loading = false;

  constructor() { }

  ngOnInit() {
  }
}
