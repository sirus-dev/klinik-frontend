import { Routes } from '@angular/router';

import { KlasifikasiComponent } from './klasifikasi.component';

export const KlasifikasiRoutes: Routes = [{
  path: '',
  component: KlasifikasiComponent,
  data: {
    breadcrumb: 'Klasifikasi',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];