import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { Location } from '@angular/common';

declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-identitas-pasien',
  templateUrl: './identitas-pasien.component.html'
})

export class IdentitasPasienComponent implements OnInit {
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>; 
  public loading = false;
  
  public show:boolean = false;
  public buttonName:any = 'Show';

  constructor( private _location: Location) {}
  
  backClicked() {
    this._location.back();
    }

  refClicked() {
    location.reload();
  }

  ngOnInit() {
  }
  toggle() {
    this.show = !this.show;

    // CHANGE THE NAME OF THE BUTTON.
    if(this.show)  
      this.buttonName = "Hide";
    else
      this.buttonName = "Show";
  }
}
