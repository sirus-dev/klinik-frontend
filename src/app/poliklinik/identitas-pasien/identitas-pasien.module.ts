import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { IdentitasPasienComponent } from './identitas-pasien.component';
import { IdentitasPasienRoutes } from './identitas-pasien.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(IdentitasPasienRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [IdentitasPasienComponent]
})

export class IdentitasPasienModule {}
