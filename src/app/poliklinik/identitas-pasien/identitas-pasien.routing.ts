import { Routes } from '@angular/router';

import { IdentitasPasienComponent } from './identitas-pasien.component';

export const IdentitasPasienRoutes: Routes = [{
  path: '',
  component: IdentitasPasienComponent,
  data: {
    breadcrumb: 'Identitas Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];