import { Routes } from '@angular/router';
import {PoliklinikComponent} from './poliklinik.component';
import {PendaftaranComponent} from './pendaftaran/pendaftaran.component'; 
import { IdentitasPasienComponent } from '../poliklinik/identitas-pasien/identitas-pasien.component';
import { TandaVitalComponent } from './tanda-vital/tanda-vital.component';
import { PemeriksaanPasienComponent } from './pemeriksaan-pasien/pemeriksaan-pasien.component';
import { PemeriksaanDokterComponent } from './pemeriksaan-dokter/pemeriksaan-dokter.component';
import { LapJumlahPasienComponent } from './lap-jumlah-pasien/lap-jumlah-pasien.component';
import { RujukLuarComponent } from './rujuk-luar/rujuk-luar.component';
import { LapRujukLuarComponent } from './lap-rujuk-luar/lap-rujuk-luar.component';
import { DaftarPoliComponent } from './daftar-poli/daftar-poli.component';



export const PoliklinikRoutes: Routes = [
  {
    path: '',
    data: {
        breadcrumb: 'Poliklinik',
        status: false
    },
    children: [
        {
            path: '',
            component: PoliklinikComponent,
            data: {
                breadcrumb: 'Poliklinik',
                status: true
            } 
        }, {
            path: 'pendaftaran',
            component: PendaftaranComponent,
            data: {
                breadcrumb: 'Pendaftaran',
                status: true
            } 
        },  {
            path: 'identitas-pasien',
            component: IdentitasPasienComponent,
            data: {
                breadcrumb: 'Daftar Identitas',
                status: true
            } 
        },  {
            path: 'tanda-vital',
            component: TandaVitalComponent,
            data: {
                breadcrumb: 'Pemeriksaan Tanda Vital',
                status: true
            } 
        },  {
            path: 'pemeriksaan-pasien',
            component: PemeriksaanPasienComponent,
            data: {
                breadcrumb: 'Pemeriksaan Pasien',
                status: true
            } 
        },  {
            path: 'pemeriksaan-dokter',
            component: PemeriksaanDokterComponent,
            data: {
                breadcrumb: 'Pemeriksaan Pasien oleh Dokter',
                status: true
            } 
        }, {
            path: 'lap-jumlah-pasien',
            component: LapJumlahPasienComponent,
            data: {
                breadcrumb: 'Laporan Jumlah Pemeriksaan Pasien',
                status: true
            } 
        }, {
            path: 'rujuk-luar',
            component: RujukLuarComponent,
            data: {
                breadcrumb: 'Rujukan Luar',
                status: true
            } 
        }, {
            path: 'lap-rujuk-luar',
            component: LapRujukLuarComponent,
            data: {
                breadcrumb: 'Laporan Rujukan Luar',
                status: true
            } 
        },  {
            path: 'daftar-poli',
            component: DaftarPoliComponent,
            data: {
                breadcrumb: 'Daftar Poliklinik',
                status: true
            } 
        },
    ]
  }
];
