import { Routes } from '@angular/router';

import { PendaftaranComponent } from './pendaftaran.component';

export const PendaftaranRoutes: Routes = [{
  path: '',
  component: PendaftaranComponent,
  data: {
    breadcrumb: 'Pendaftaran',
    icon: 'icofont-home bg-c-blue',
    status: false
  },
  children: [
        {
          path: '',
          component: PendaftaranComponent,
          data: {
              breadcrumb: 'Pendaftaran',
              status: true
          } 
      },
    ]
  }
];
