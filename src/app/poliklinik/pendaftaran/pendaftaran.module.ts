import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PendaftaranComponent } from './pendaftaran.component';
import { PendaftaranRoutes } from './pendaftaran.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PendaftaranRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [
    PendaftaranComponent,
  ]
})

export class PendaftaranModule {}
