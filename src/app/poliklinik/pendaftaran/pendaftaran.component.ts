import {Component, ViewChild, TemplateRef, OnInit} from '@angular/core';
import { NgxLoadingComponent, ngxLoadingAnimationTypes } from 'ngx-loading';
import { Location } from '@angular/common';

declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-pendaftaran',
  templateUrl: './pendaftaran.component.html'
})

export class PendaftaranComponent implements OnInit {
  @ViewChild('ngxLoading') ngxLoadingComponent: NgxLoadingComponent;
  @ViewChild('customLoadingTemplate') customLoadingTemplate: TemplateRef<any>; 
  public loading = false;

  constructor( private _location: Location) {}
  
  backClicked() {
    this._location.back();
    }

  refClicked() {
    location.reload();
  }

  ngOnInit() {
  }
}