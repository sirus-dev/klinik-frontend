import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { LapRujukLuarComponent } from './lap-rujuk-luar.component';
import { LapRujukLuarRoutes } from './lap-rujuk-luar.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(LapRujukLuarRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [LapRujukLuarComponent]
})

export class LapRujukLuarModule {}
