import { Routes } from '@angular/router';

import { LapRujukLuarComponent } from './lap-rujuk-luar.component';

export const LapRujukLuarRoutes: Routes = [{
  path: '',
  component: LapRujukLuarComponent,
  data: {
    breadcrumb: 'Laporan Rujukan Luar',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];