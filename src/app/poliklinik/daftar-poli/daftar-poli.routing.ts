import { Routes } from '@angular/router';

import { DaftarPoliComponent } from './daftar-poli.component';

export const DaftarPoliRoutes: Routes = [{
  path: '',
  component: DaftarPoliComponent,
  data: {
    breadcrumb: 'Identitas Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];