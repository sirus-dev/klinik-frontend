import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DaftarPoliComponent } from './daftar-poli.component';
import { DaftarPoliRoutes } from './daftar-poli.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(DaftarPoliRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [DaftarPoliComponent]
})

export class DaftarPoliModule {}
