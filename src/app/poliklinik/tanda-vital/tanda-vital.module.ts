import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TandaVitalComponent } from './tanda-vital.component';
import { TandaVitalRoutes } from './tanda-vital.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(TandaVitalRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [TandaVitalComponent]
})

export class TandaVitalModule {}
