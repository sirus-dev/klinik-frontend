import { Routes } from '@angular/router';

import { TandaVitalComponent } from './tanda-vital.component';

export const TandaVitalRoutes: Routes = [{
  path: '',
  component: TandaVitalComponent,
  data: {
    breadcrumb: 'Identitas Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];