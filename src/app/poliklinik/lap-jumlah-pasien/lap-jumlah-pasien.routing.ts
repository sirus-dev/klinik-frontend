import { Routes } from '@angular/router';

import { LapJumlahPasienComponent } from './lap-jumlah-pasien.component';

export const LapJumlahPasienRoutes: Routes = [{
  path: '',
  component: LapJumlahPasienComponent,
  data: {
    breadcrumb: 'Identitas Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];