import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { LapJumlahPasienComponent } from './lap-jumlah-pasien.component';
import { LapJumlahPasienRoutes } from './lap-jumlah-pasien.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(LapJumlahPasienRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [LapJumlahPasienComponent]
})

export class LapJumlahPasienModule {}
