import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PemeriksaanPasienComponent } from './pemeriksaan-pasien.component';
import { PemeriksaanPasienRoutes } from './pemeriksaan-pasien.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PemeriksaanPasienRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PemeriksaanPasienComponent]
})

export class PemeriksaanPasienModule {}
