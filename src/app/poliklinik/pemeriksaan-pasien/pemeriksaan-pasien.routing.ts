import { Routes } from '@angular/router';

import { PemeriksaanPasienComponent } from './pemeriksaan-pasien.component';

export const PemeriksaanPasienRoutes: Routes = [{
  path: '',
  component: PemeriksaanPasienComponent,
  data: {
    breadcrumb: 'Identitas Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];