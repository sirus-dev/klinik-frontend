import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PoliklinikComponent } from './poliklinik.component';
import { PoliklinikRoutes } from './poliklinik.routing';
import { PendaftaranComponent } from './pendaftaran/pendaftaran.component';
import { IdentitasPasienComponent } from './identitas-pasien/identitas-pasien.component';
import { SharedModule} from '../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';
import { TandaVitalComponent } from './tanda-vital/tanda-vital.component';
import { PemeriksaanPasienComponent } from './pemeriksaan-pasien/pemeriksaan-pasien.component';
import { PemeriksaanDokterComponent } from './pemeriksaan-dokter/pemeriksaan-dokter.component';
import { LapJumlahPasienComponent } from './lap-jumlah-pasien/lap-jumlah-pasien.component';
import { RujukLuarComponent } from './rujuk-luar/rujuk-luar.component';
import { LapRujukLuarComponent } from './lap-rujuk-luar/lap-rujuk-luar.component';
import { DaftarPoliComponent } from './daftar-poli/daftar-poli.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PoliklinikRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [
    PoliklinikComponent,
    PendaftaranComponent,
    IdentitasPasienComponent,
    TandaVitalComponent,
    PemeriksaanPasienComponent,
    PemeriksaanDokterComponent,
    LapJumlahPasienComponent,
    RujukLuarComponent,
    LapRujukLuarComponent,
    DaftarPoliComponent
  ]
})

export class PoliklinikModule {}
