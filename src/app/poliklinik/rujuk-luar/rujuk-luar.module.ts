import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RujukLuarComponent } from './rujuk-luar.component';
import { RujukLuarRoutes } from './rujuk-luar.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RujukLuarRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [RujukLuarComponent]
})

export class RujukLuarModule {}
