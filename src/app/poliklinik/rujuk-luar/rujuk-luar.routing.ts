import { Routes } from '@angular/router';

import { RujukLuarComponent } from './rujuk-luar.component';

export const RujukLuarRoutes: Routes = [{
  path: '',
  component: RujukLuarComponent,
  data: {
    breadcrumb: 'Laporan Rujukan Luar',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];