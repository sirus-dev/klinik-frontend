import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PemeriksaanDokterComponent } from './pemeriksaan-dokter.component';
import { PemeriksaanDokterRoutes } from './pemeriksaan-dokter.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PemeriksaanDokterRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PemeriksaanDokterComponent]
})

export class PemeriksaanDokterModule {}
