import { Routes } from '@angular/router';

import { PemeriksaanDokterComponent } from './pemeriksaan-dokter.component';

export const PemeriksaanDokterRoutes: Routes = [{
  path: '',
  component: PemeriksaanDokterComponent,
  data: {
    breadcrumb: 'Identitas Pasien',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];