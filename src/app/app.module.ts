import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";

import { AppRoutes } from "./app.routing";
import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin/admin-layout.component";
import { MainLayoutComponent } from "./layouts/main/main-layout.component";
import { ProfileLayoutComponent } from "./layouts/profile/profile-layout.component";
import { SettingLayoutComponent } from "./layouts/setting/setting-layout.component";
import { AuthLayoutComponent } from "./layouts/auth/auth-layout.component";
import { SharedModule } from "./shared/shared.module";
import { BreadcrumbsComponent } from "./layouts/admin/breadcrumbs/breadcrumbs.component";
import { TitleComponent } from "./layouts/admin/title/title.component";
import { ScrollModule } from "./shared/scroll/scroll.module";
import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { SubMenuComponent } from "./layouts/sub-menu/sub-menu.component";
import { AdministratorComponent } from './layouts/administrator/administrator.component'

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    MainLayoutComponent,
    ProfileLayoutComponent,
    SettingLayoutComponent,
    AuthLayoutComponent,
    BreadcrumbsComponent,
    TitleComponent,
    SubMenuComponent,
    AdministratorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    HttpModule,
    ScrollModule
  ],
  exports: [ScrollModule, SubMenuComponent],
  providers: [{ provide: LocationStrategy, useClass: PathLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule {}
