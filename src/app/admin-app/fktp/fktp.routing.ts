import { Routes } from '@angular/router';

import { FKTPComponent } from './fktp.component';

export const FKTPRoutes: Routes = [{
  path: '',
  component: FKTPComponent,
  data: {
    breadcrumb: 'Data Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];