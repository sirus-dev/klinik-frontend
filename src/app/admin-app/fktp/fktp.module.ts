import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FKTPComponent } from './fktp.component';
import { FKTPRoutes } from './fktp.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(FKTPRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [FKTPComponent]
})

export class FKTPModule {}