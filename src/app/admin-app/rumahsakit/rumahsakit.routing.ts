import { Routes } from '@angular/router';

import { RumahsakitComponent } from './rumahsakit.component';

export const RumahsakitRoutes: Routes = [{
  path: '',
  component: RumahsakitComponent,
  data: {
    breadcrumb: 'Data Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];