import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahsakitComponent } from './rumahsakit.component';
import { RumahsakitRoutes } from './rumahsakit.routing';
import { SharedModule } from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahsakitRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [RumahsakitComponent]
})

export class RumahsakitModule {}