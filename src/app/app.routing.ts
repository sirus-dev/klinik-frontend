import { Routes } from "@angular/router";

// import { AdminLayoutComponent } from "./layouts/admin/admin-layout.component";
import { MainLayoutComponent } from "./layouts/main/main-layout.component";
import { ProfileLayoutComponent } from "./layouts/profile/profile-layout.component";
import { SettingLayoutComponent } from "./layouts/setting/setting-layout.component";
import { AuthLayoutComponent } from "./layouts/auth/auth-layout.component";
import { AdministratorComponent } from "./layouts/administrator/administrator.component";

export const AppRoutes: Routes = [
  { path: "", redirectTo: "login", pathMatch: "full" },
  {
    path: "",
    component: MainLayoutComponent,
    children: [
      { 
        path: "", 
        redirectTo: "dashboard", 
        pathMatch: "full" 
      },
      {
        path: "dashboard",
        loadChildren: "./dashboard/dashboard.module#DashboardModule"
      }, 
      {
        path: "poliklinik",
        loadChildren: "./poliklinik/poliklinik.module#PoliklinikModule"
      },
      {
        path: "rekam-medis",
        loadChildren: "./rekam-medis/rekam-medis.module#RekamMedisModule"
      },
      {
        path: "master",
        loadChildren: "./master/master.module#MasterModule"
      },
      {
        path: "apotek",
        loadChildren: "./apotek/apotek.module#ApotekModule"
      }, 
    ]
  },
  {
    path: "profile",
    component: ProfileLayoutComponent,
    children: [
      { 
        path: "", 
        loadChildren: "./profile/profile.module#ProfileModule"
      }, 
    ]
  },
  {
    path: "setting",
    component: SettingLayoutComponent,
    children: [
      { 
        path: "", 
        redirectTo: "klinik", 
        pathMatch: "full" 
      },
      {
        path: "klinik",
        loadChildren: "./setting-menu/klinik/klinik.module#KlinikModule"
      },
      {
        path: "rumah-sakit",
        loadChildren: "./setting-menu/rumah-sakit/rumah-sakit.module#RumahSakitModule"
      },
      {
        path: "pengguna",
        loadChildren: "./setting-menu/pengguna/pengguna.module#PenggunaModule"
      }, 
      {
        path: "hak-akses",
        loadChildren: "./setting-menu/hak-akses/hak-akses.module#HakAksesModule"
      }, 
      {
        path: "klasifikasi",
        loadChildren: "./setting-menu/klasifikasi/klasifikasi.module#KlasifikasiModule"
      }, 
    ]
  },
  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      { 
        path: "", 
        redirectTo: "login", 
        pathMatch: "full" },
      {
        path: "login",
        loadChildren:"./authentication/login-email/login-email.module#LoginEmailModule"
      },
      {
        path: "login-password",
        loadChildren:"./authentication/login-password/login-password.module#LoginPasswordModule"
      },
      {
        path: "forgot",
        loadChildren: "./authentication/forgot/forgot.module#ForgotModule"
      },
      { 
        path: "error", 
        loadChildren: "./error/error.module#ErrorModule" 
      }
    ]
  },
  {
    path: "administrator",
    component: AdministratorComponent,
    children: [
      { 
        path: "", 
        redirectTo: "fktp", 
        pathMatch: "full" },
      {
        path: "fktp",
        loadChildren:"./admin-app/fktp/fktp.module#FKTPModule"
      },
      {
        path: "rumahsakit",
        loadChildren:"./admin-app/rumahsakit/rumahsakit.module#RumahsakitModule"
      },
    ]
  },
  { path: "**", redirectTo: "error/404" }
];
