import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PenggunaComponent } from './pengguna.component';
import { PenggunaRoutes } from './pengguna.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(PenggunaRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [PenggunaComponent]
})

export class PenggunaModule {}
