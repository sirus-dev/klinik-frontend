import { Routes } from '@angular/router';

import { PenggunaComponent } from './pengguna.component';

export const PenggunaRoutes: Routes = [{
  path: '',
  component: PenggunaComponent,
  data: {
    breadcrumb: 'Pengguna',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
