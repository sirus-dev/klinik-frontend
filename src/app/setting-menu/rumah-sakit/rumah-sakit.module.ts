import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { RumahSakitComponent } from './rumah-sakit.component';
import { RumahSakitRoutes } from './rumah-sakit.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(RumahSakitRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [RumahSakitComponent]
})

export class RumahSakitModule {}
