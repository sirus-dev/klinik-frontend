import { Routes } from '@angular/router';

import { RumahSakitComponent } from './rumah-sakit.component';

export const RumahSakitRoutes: Routes = [{
  path: '',
  component: RumahSakitComponent,
  data: {
    breadcrumb: 'Setting Rumah Sakit',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
