import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlasifikasiComponent } from './klasifikasi.component';
import { KlasifikasiRoutes } from './klasifikasi.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlasifikasiRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [KlasifikasiComponent]
})

export class KlasifikasiModule {}
