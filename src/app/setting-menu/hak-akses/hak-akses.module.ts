import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { HakAksesComponent } from './hak-akses.component';
import { HakAksesRoutes } from './hak-akses.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(HakAksesRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [HakAksesComponent]
})

export class HakAksesModule {}
