import { Routes } from '@angular/router';

import { HakAksesComponent } from './hak-akses.component';

export const HakAksesRoutes: Routes = [{
  path: '',
  component: HakAksesComponent,
  data: {
    breadcrumb: 'HakAkses',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
