import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { KlinikComponent } from './klinik.component';
import { KlinikRoutes } from './klinik.routing';
import {SharedModule} from '../../shared/shared.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
      CommonModule,
      RouterModule.forChild(KlinikRoutes),
      SharedModule,
      NgxLoadingModule.forRoot({})
  ],
  declarations: [KlinikComponent]
})

export class KlinikModule {}
