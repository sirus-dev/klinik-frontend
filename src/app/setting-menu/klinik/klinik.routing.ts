import { Routes } from '@angular/router';

import { KlinikComponent } from './klinik.component';

export const KlinikRoutes: Routes = [{
  path: '',
  component: KlinikComponent,
  data: {
    breadcrumb: 'Setting Klinik',
    icon: 'icofont-home bg-c-blue',
    status: false
  }
}];
